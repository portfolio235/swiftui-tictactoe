//
//  GameSquareView.swift
//  TicTacToe-SwiftUI
//
//  Created by Massimiliano Bonafede on 16/09/22.
//

import SwiftUI

struct GameSquareView: View {
    var geometry: GeometryProxy
    
    var body: some View {
        Circle()
            .foregroundColor(.red)
            .opacity(0.5)
            .frame(
                width: geometry.size.width / 3 - 15,
                height: geometry.size.width / 3 - 15)
    }
}
