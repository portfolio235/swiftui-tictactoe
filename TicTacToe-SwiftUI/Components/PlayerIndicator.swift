//
//  PlayerIndicator.swift
//  TicTacToe-SwiftUI
//
//  Created by Massimiliano Bonafede on 16/09/22.
//

import SwiftUI

struct PlayerIndicator: View {
    var image: String?
    
    var body: some View {
        Image(systemName: image ?? "")
            .resizable()
            .frame(width: 40, height: 40)
            .foregroundColor(.white)
    }
}
