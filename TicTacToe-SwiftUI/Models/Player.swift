//
//  Player.swift
//  TicTacToe-SwiftUI
//
//  Created by Massimiliano Bonafede on 16/09/22.
//

import Foundation

enum Player {
    case human
    case computer
}
