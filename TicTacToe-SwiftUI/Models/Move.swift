//
//  Move.swift
//  TicTacToe-SwiftUI
//
//  Created by Massimiliano Bonafede on 16/09/22.
//

import Foundation

struct Move {
    let player: Player
    let boardIndex: Int
    
    var indicator: String {
        return player == .human ? "xmark" : "circle"
    }
}
