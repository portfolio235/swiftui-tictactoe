//
//  TicTacToe_SwiftUIApp.swift
//  TicTacToe-SwiftUI
//
//  Created by Massimiliano Bonafede on 06/09/22.
//

import SwiftUI

@main
struct TicTacToe_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            GameView()
        }
    }
}

struct Previews_TicTacToe_SwiftUIApp_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
