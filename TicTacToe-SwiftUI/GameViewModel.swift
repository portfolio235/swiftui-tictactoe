//
//  GameViewModel.swift
//  TicTacToe-SwiftUI
//
//  Created by Massimiliano Bonafede on 16/09/22.
//

import SwiftUI

final class GameViewModel: ObservableObject {
    let columns: [GridItem] = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]
    
    @Published var moves: [Move?] = Array(repeating: nil, count: 9)
    @Published var isBoardDisabled: Bool = false
    @Published var alertItem: AlertItem?
    
    
    func processPlayerMove(for position: Int) {
        guard isOccupied(in: moves, for: position) == false else { return }
        moves[position] = Move(player: .human, boardIndex: position)
        
        
        if checkWin(for: .human, in: moves) {
            alertItem = AlertContext.humanWin
            return
        }
        
        if checkForDraw(in: moves) {
            alertItem = AlertContext.draw
            return
        }
        
        isBoardDisabled = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            guard let self = self else { return }
            let computerPosition = self.determineComputerMovePosition(in: self.moves)
            self.moves[computerPosition] = Move(player: .computer, boardIndex: computerPosition)
            
            self.isBoardDisabled = false
            
            if self.checkWin(for: .computer, in: self.moves) {
                self.alertItem = AlertContext.computerWin
                return
            }
            
            if self.checkForDraw(in: self.moves) {
                self.alertItem = AlertContext.draw
                return
            }
        }
    }
    
    
    func isOccupied(in moves: [Move?], for index: Int) -> Bool {
        return moves.contains(where: { $0?.boardIndex == index })
    }
    
    
    // If AI can win, then win
    // If AI can't win, then block
    // If AI can't block, then take middle square
    // If AI can't take the middle square, take random available square
    func determineComputerMovePosition(in position: [Move?]) -> Int {
        // If AI can win, then win
        let winPatterns: Set<Set<Int>> = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]]
        
        let computerMoves = moves.compactMap { $0 }.filter { $0.player == .computer }
        let computerPosition = Set(computerMoves.map { $0.boardIndex })
        
        for pattern in winPatterns {
            let winPosition = pattern.subtracting(computerPosition)
            
            if winPosition.count == 1 {
                let isAvailable = !isOccupied(in: moves, for: winPosition.first!)
                
                if isAvailable {
                    return winPosition.first!
                }
            }
        }
        
        // If AI can't win, then block
        let humanMoves = moves.compactMap { $0 }.filter { $0.player == .human }
        let humanPosition = Set(humanMoves.map { $0.boardIndex })
        
        for pattern in winPatterns {
            let winPosition = pattern.subtracting(humanPosition)
            
            if winPosition.count == 1 {
                let isAvailable = !isOccupied(in: moves, for: winPosition.first!)
                
                if isAvailable {
                    return winPosition.first!
                }
            }
        }
        
        // If AI can't block, then take middle square
        let centerCircle = 4
        if !isOccupied(in: moves, for: centerCircle) {
            return centerCircle
        }
        
        // If AI can't take the middle square, take random available square
        var movePosition = Int.random(in: 0..<9)
        
        while isOccupied(in: moves, for: movePosition) {
            movePosition = Int.random(in: 0..<9)
        }
        
        return movePosition
    }
    
    func checkWin(for player: Player, in moves: [Move?]) -> Bool {
        let winPatterns: Set<Set<Int>> = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]]
        
        let playerMoves = moves.compactMap { $0 }.filter { $0.player == player }
        let playerPosition = Set(playerMoves.map { $0.boardIndex })
        
        for pattern in winPatterns where pattern.isSubset(of: playerPosition) { return true }
        
        return false
    }
    
    func checkForDraw(in moves: [Move?]) -> Bool {
        return moves.compactMap { $0 }.count == 9
    }
    
    func reset() {
        moves = Array(repeating: nil, count: 9)
    }
}
